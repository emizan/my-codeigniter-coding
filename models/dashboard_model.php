<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Dashboard_model extends CI_Model {
	function __construct() {
		parent::__construct ();
	}
	
	/*
	 * perform get data
	 */
	function get($tableNameSelectFields, $selectField, $whereArray = array()) {
		$this->db->select ( $selectField );
		
		if (is_array ( $whereArray ) && count ( $whereArray ) > 0) {
			
			$this->db->where ( $whereArray );
		}
		
		$selectData = $this->db->get ( $tableNameSelectFields );
		
		return $selectData;
	}
	
	/*
	 * perform data listing
	 */
	function listing($xmlData, $tableField = array("id"), $condition = "", $allOrderBy, $orderByDirection, $orderByField, $limit_start = 0, $limit_items = 10, $additional_condition = '') {
		$maintableName = $this->config->item ( "prefix" ) . $xmlData ['name'];
		
		$primary_key = $xmlData ['primary_key'];
		
		if (strlen ( $orderByDirection )) {
			$object = new stdClass ();
			$object->direction = $orderByDirection;
			$array_key = (string) $orderByField; 
			
			$allOrderBy = array_merge ( array (
					$array_key => $object 
			), $allOrderBy );
		}
		
		foreach ( $xmlData->field as $value ) {
			
			$tableFieldName = ( string ) $value ['name'];
			
			$lookup = false;
			
			if (in_array ( $tableFieldName, $tableField )) {
				
				if (isset ( $value ['type'] ) && $value ['type'] == 'lookup') {
					
					$tableNameJoin = $this->config->item ( "prefix" ) . ( string ) $value ['ref_table'];
					
					$reference_table_name = $tableFieldName.'_ref';
					
					$joinTableKey = ( string ) $value ['key'];
					
					$joinTableValue = ( string ) $value ['value'];
					
					$joinCondition = $reference_table_name . '.' . $joinTableKey . ' = ' . $maintableName . '.' . $tableFieldName;
					
					$selectValue = "{$reference_table_name}.{$joinTableValue} AS {$tableFieldName}";
					
					$this->db->select ( $selectValue );
					
					$this->db->join ( $tableNameJoin.' '.$reference_table_name, $joinCondition, 'LEFT' );
					
					$lookup = true;
					
					if (isset ( $condition ) && $condition) {
						
						$this->db->or_like ( $reference_table_name . '.' . $joinTableValue, $condition );
					}
				} else {
					
					$this->db->select ( $maintableName . '.' . $tableFieldName );
					if (isset ( $condition ) && $condition) {
						
						$this->db->or_like ( $maintableName . '.' . $tableFieldName, $condition );
					}
				}
			}
			
			if (array_key_exists ( $tableFieldName, $allOrderBy )) {
				
				if ($lookup) {
					
					$allOrderBy [$tableFieldName]->fullName = $reference_table_name . '.' . $joinTableValue;
				} else {
					
					$allOrderBy [$tableFieldName]->fullName = $maintableName . '.' . $tableFieldName;
				}
			}
		}
		foreach ( $allOrderBy as $key => $value ) {
			
			if (isset ( $value->fullName ) && $value->fullName) {
				
				$this->db->order_by ( $value->fullName, $value->direction );
			}
		}
		/*
		 * additional condition added by any specific application which requires some extra condition
		 */
		if (is_array ( $additional_condition )) {
			
			if (count ( $additional_condition )) {
				
				if(isset($additional_condition ['AND'])){
					
					if ( is_array ( $additional_condition ['AND'] ) && count ( $additional_condition ['AND'] )) {
						
						$this->db->where ( '( ' . implode ( ' AND ', $additional_condition ['AND'] ) . ' )' );
						
					} else if (strlen ( $additional_condition ['AND'] )) {
						
						$this->db->where ( $additional_condition ['AND'] );
					}
				}
				if(isset($additional_condition ['OR'] )){
					
					if (is_array ( $additional_condition ['OR'] ) && count ( $additional_condition ['OR'] )) {
						
						$this->db->or_where ( '( ' . implode ( ' OR ', $additional_condition ['OR'] ) . ' )' );
						
					} else if (strlen ( $additional_condition ['OR'] )) {
						
						$this->db->or_where ( $additional_condition ['OR'] );
					}
				}
			}
		}
		
		$this->db->select ( $maintableName . '.' . $primary_key );
		
		$query = $this->db->get ( $maintableName, $limit_items, $limit_start );
		$last_query =  $this->db->last_query();
		$dashboard_helper = Dashboard_main_helper::get_instance();
		
		if( ( $limit_position =  strrpos($last_query , 'LIMIT' )) !== false ){
			$total_count_query =  substr($last_query , 0, $limit_position) ;
			$total_items = $this->db->query($total_count_query)->num_rows() ;
			$dashboard_helper->set('listing_total_items', $total_items );
		}else{
			$dashboard_helper->set('listing_total_items', $query->num_rows() );
		}

		return $query->result ();
	}
	
	/*
	 * perform add
	 */
	function add($tableName, $data) {
		$data_insert = $this->db->insert ( $tableName, $data );
		
		return $data_insert;
	}
	
	/*
	 * perform edit action
	 */
	function edit($tableName, $data, $whereArray = array()) {
		if (is_array ( $whereArray ) && count ( $whereArray ) > 0) {
			
			$this->db->where ( $whereArray );
		}
		
		$result_update = $this->db->update ( $tableName, $data );
		
		return $result_update;
	}
	
	/*
	 * perform delete data
	 */
	function delete($tableName, $fields) {
		$queryDelete = "DELETE FROM " . $tableName . " WHERE id IN(" . $fields . ")";
		
		$query = $this->db->query ( $queryDelete );
		
		return $query;
	}
	
	/*
	 * perform copy items
	 */
	function copy($tableName, $idsArray, $copy_field = "") {
		foreach ( $idsArray as $id ) {
			
			$this->db->select ( "*" );
			$this->db->where ( "id", $id );
			$selectQuery = $this->db->get ( $tableName );
			
			$data = $selectQuery->row ();
			
			unset ( $data->id );
			
			if (isset ( $copy_field ) && $copy_field) {
				
				$data->$copy_field = dashboard_lang ( '_COPY_DATA_TO_SAVE_DATABASE' ) . $data->{$copy_field};
			}
			
			$result = $this->db->insert ( $tableName, $data );
		}
		
		return $result;
	}
	
	/*
	 * Table rows exist checking
	 */
	public function tableTotalRows($tableName, $tableField = array("id"), $condition = "") {
		if (isset ( $condition ) && $condition) {
			
			foreach ( $tableField as $field ) {
				
				$this->db->or_like ( $field, $condition );
			}
		}
		
		$queryResult = $this->db->get ( $tableName );
		
		return $queryResult->num_rows ();
	}
	
	/*
	 * Search auto suggest words here
	 */
	public function get_words($tableName, $tableField = array("id"), $condition = "", $limit = 10) {
		$finalResult = array ();
		$condition = $this->db->escape_like_str ( $condition );
		
		if (isset ( $condition ) && $condition) {
			
			$querySelect = array ();
			
			foreach ( $tableField as $field ) {
				
				if (strlen ( $field )) {
					
					$querySelect [] = "SELECT `" . $field . "` AS value FROM " . $tableName . " WHERE `" . $field . "` LIKE '%" . $condition . "%' ";
				}
			}
			
			$query = implode ( ' UNION ', $querySelect );
			
			$query .= " LIMIT " . $limit;
			
			$query = $this->db->query ( $query );
			
			$finalResult = $query->result ();
		}
		
		return $finalResult;
	}
	
	
	function get_autosuggest_lookup($tableName, $tableField, $wordWhere, $orderBy, $orderOn, $limit = 10){

		$finalResult = array();
		
		if(is_array($wordWhere) && count($wordWhere) > 0){
			$this->db->select($tableField);
			$this->db->like($wordWhere);
			$this->db->order_by($orderBy, $orderOn);
			
			$query = $this->db->get($tableName);
			$finalResult = $query->result();
				
		}

		return $finalResult;
	}
	
	/*
	 * lookup select form building function
	 */
	public function lookup($tableName, $key, $value, $orderBy, $orderOn) {
		$select = $key . ',' . $value;
		$this->db->select ( $select );
		$this->db->order_by($orderBy, $orderOn);
		$query = $this->db->get ( $tableName );
		
		return $query;
	}
	public function getUserGroupQuery($tableName) {
		$this->db->select ( "*" );
		
		$this->db->where ( "table", $tableName );
		$this->db->where ( "role", get_user_role () );
		$query = $this->db->get ( $this->config->item ( 'prefix' ) . "table_permissions" );
		
		return $query;
	}
	public function getSupperUserPermission($edit = '') {
		$this->db->select ( "*" );
		$this->db->where ( "table", "*" );
		$this->db->where ( "field_name", "*" );
		if ($edit) {
			$this->db->where ( "edit", $edit );
		}
		$this->db->where ( "role", get_user_role () );
		$query = $this->db->get ( $this->config->item ( 'prefix' ) . "table_permissions" );
		
		if( $query->num_rows() ){
			return $query->row() ;
		}else{
			return 0 ;
		}
	}

	/*
	 * Table rows exist or not checking
	 */

	public function getTableRows($tableName, $selectField, $whereArray = ""){
	
		$this->db->select($selectField);
	
		if(is_array($whereArray) && count($whereArray) > 0){
				
			$this->db->where($whereArray);
		}
	
		$selectData = $this->db->get($tableName);;
	
		return $selectData->num_rows();
	}
	
}


/* End of file: Dashboard_model.php */
/* Location: ./admin/application/core/Dashboard_model.php */