<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Dashboard_controller extends CI_Controller {
	public $tablePrefix;
	public $tableName;
	public $_xmldata;
	function __construct() {
		parent::__construct ();
		
		$this->load->database ();
		$this->load->library ( 'session' );
		$this->load->library ( 'template' );
		
		$this->config->load ( 'dashboard' );
		$this->config->load ( 'dashboard_override', FALSE, TRUE );
		
		$this->load->helper ( 'boo2' );
		$this->load->helper ( 'dashboard_main' );
		$this->load->helper ( 'dashboard_menu' );
		$this->load->helper ( 'language' );
		
		$user = get_user_id ();
		
		$user_role = get_user_role ();
		
		if (! $user) {
			redirect ( site_url () . $this->config->item ( 'login_url' ) );
		}
		
		$this->load->model ( "../core/dashboard_model" );
		
		$this->lang->load ( 'dashboard' );
		$this->lang->load ( 'dashboard_override' );
		
		// template
		$this->template->set_template ( $this->config->item ( 'template_name' ) );
		$this->tablePrefix = $this->config->item ( "prefix" );
	}
	
	/*
	 * perform listing
	 */
	public function listing() {
		$this->load->helper ( 'dashboard_list' );
		$supperUserPermission = $this->dashboard_model->getSupperUserPermission ();
		$dashboard_helper = Dashboard_main_helper::get_instance ();
		$dashboard_helper->set ( "supper_user", 0 );
		$supper_user = 0 ;
		if(is_object($supperUserPermission) ){
			$dashboard_helper->set ( "supper_user", 1 );
			$supper_user = 1 ;
		}else{
			$tableName = $this->current_class_name;
			$userAccess = $this->dashboard_model->getUserGroupQuery ( $tableName );
			
			if($userAccess->num_rows() > 0){
				$dashboard_helper->set ( "supper_user", 1 );
				$supper_user = 1 ;
			}
		}
		
		
		$controller_sub_folder = $this->config->item ( 'controller_sub_folder' );
		
		
		/*
		 * input: group_name , table_name return: list of fields which will be visible in the system
		 */
		
		$this->load->library ( 'pagination' );
		
		$listingPath = $controller_sub_folder . '/' . $this->current_class_name . "/" . $this->config->item ( "listing_view" );
		
		$this->session->set_userdata ( "current_url", $listingPath );
		
		$xmlFile = FCPATH . $this->config->item ( "xml_file_path" ) . DIRECTORY_SEPARATOR . $this->current_class_name . ".xml";
		
		$this->config->load ( 'dashboard_override', FALSE, TRUE );
		
		$xmlData = $this->xml_parsing ();
		
		$data ['xmlData'] = $xmlData;
		
		$primary_key = (string) $xmlData ['primary_key'];
		
		if ($primary_key == "") {
						
			show_error( dashboard_lang('PRIMARY_KEY_MISSING') );

			return FALSE;
		}		
		
		$perPegeArray = $this->config->item ( "list_per_page" );
		
		$per_page = $this->input->post ( "per_page" );
		
		$sessionPerPage = $this->session->userdata ( 'per_page' );
		
		if (isset ( $sessionPerPage ) && $sessionPerPage != "" && (($sessionPerPage == $per_page) || ($per_page == ""))) {
			$per_page = $this->session->userdata ( 'per_page' );
		} else {
			if (isset ( $per_page ) && $per_page != "") {
				$this->session->set_userdata ( 'per_page', $per_page );
			} else {
				$per_page = current ( $perPegeArray );
			}
		}
		
		$limit_items = $per_page;
		
		$tableName = $this->tablePrefix . ( string ) $xmlData ['name'];
		$tableNameFixed = ( string ) $xmlData ['name'];
		$tableField = array ();
		$tableFieldOrder = array ();
		$order_by = $this->_getOrderBy($primary_key);

		$orderByField = $order_by ['table_sort_field'];
		$orderByValue = $order_by ['table_sort_direction'];
		
		/*
		 * GET user permission data
		 */
		$userPermissionDataArray = $this->getUserPermissionDataArray ( $tableNameFixed );
		
		if(array_key_exists('*' , $userPermissionDataArray) ){
			
			$supper_user=1;
			
		}		
		
		$tableNameSelectFields = $this->tablePrefix . $this->config->item ( "user_show_listing_field_table" );
		
		$selectField = "list_fields, order_fields";
		$whereArray = array (
				"user_id" => get_user_id (),
				"table_name" => $tableNameFixed 
		);
		
		$querySelectFields = $this->dashboard_model->get ( $tableNameSelectFields, $selectField, $whereArray );
		
		if ($querySelectFields->num_rows () > 0) {
			$row = $querySelectFields->row ();
			
			if (strlen ( $row->list_fields )) {
				$tableFieldDb = explode ( ",", $row->list_fields );
				$data ['listing_field'] = $tableFieldDb;
				$tableField = array_merge ( $tableField, $tableFieldDb );
			} else {
				
				foreach ( $xmlData->field as $value ) {
					if (isset ( $value ['show'] ) && intval ( $value ['show'] ) > 0) {
						$tableField [intval ( $value ['show'] )] = ( string ) $value ['name'];
					}
				}
				
				ksort ( $tableField );
				
				$data ['listing_field'] = $tableField;
			}
			
			if (strlen ( $row->order_fields )) {
				$tableFieldOrderDb = explode ( ",", $row->order_fields );
				$data ['ordering_fields'] = $tableFieldOrderDb;
				$allOrderby = implode ( ",", $tableFieldOrderDb );
			} else {
				
				foreach ( $xmlData->field as $value ) {
					if (isset ( $value ['sort'] ) && $value ['sort'] == 1) {
						$tableFieldOrder [] = ( string ) $value ['name'];
					}
				}
				
				$data ['ordering_fields'] = $tableFieldOrder;
				$allOrderby = implode ( ",", $tableFieldOrder );
			}
		} else {
			foreach ( $xmlData->field as $value ) {
				if (isset ( $value ['show'] ) && intval ( $value ['show'] ) > 0) {
					$tableField [intval ( $value ['show'] )] = ( string ) $value ['name'];
				}
				if (isset ( $value ['sort'] ) && $value ['sort'] == 1) {
					$tableFieldOrder [] = ( string ) $value ['name'];
				}
			}
			
			ksort ( $tableField );
			
			$data ['listing_field'] = $tableField;
			$data ['ordering_fields'] = $tableFieldOrder;
			$allOrderby = implode ( ",", $tableFieldOrder );
		}
		
		foreach ( $xmlData->field as $value ) {
			
			if (! $supper_user) {
				if (is_array ( $userPermissionDataArray ) && count ( $userPermissionDataArray ) > 0 && $user_view_permission != "*") {
					$data_value = ( string ) $value ['name'];
					if (array_key_exists ( $data_value, $userPermissionDataArray )) {
						
						$permitted_fields = array_keys ($userPermissionDataArray);
						$allField [( string ) $value ['name']] = $value;
						
						$tableField = array_intersect ( $tableField, $permitted_fields );
						$data ['listing_field'] = array_intersect ( $data ['listing_field'], $permitted_fields );
						$data ['ordering_fields'] = array_intersect ( $data ['ordering_fields'], $permitted_fields );
						$allOrderby = implode ( ",", $permitted_fields );
					}
				} else {
					$allFieldBuffer [( string ) $value ['name']] = $value;
				}
			} else {
				$allField [( string ) $value ['name']] = $value;
			}
		}
		
		if (! $supper_user) {
			if (count ( $userPermissionDataArray ) == 0) {
				$allField = $allFieldBuffer;
			}
			
			if ($user_view_permission == "*") {
				$allField = $allFieldBuffer;
			}
		}
		
		// all field order by operation
		if (isset ( $orderByField ) && $orderByField) {
			
			$allOrderbyArray = explode ( ",", $allOrderby );

		}
		
		$fields_sorting_saved_values = array ();
		
		foreach ( $allOrderbyArray as $key => $value ) {
			$object = new stdClass ();
			$object->direction = $this->get_userstate ( $value );
			$fields_sorting_saved_values [$value] = $object;
		}
		
		$orderByDirection = ($orderByValue != "") ? $orderByValue : $this->config->item ( "all_order_by_value" );
		
		$start = $this->uri->segment ( 4 );
		
		if ($start == NULL)
			$limit_start = 0;
		else
			$limit_start = $start;
			
			/* search start */
		
		$search = rtrim( trim( $this->input->post ( "search" ) )  , '.');
		$reset = $this->input->post ( "reset" );
		$additional_condition = $this->db_getWhereCondition ();
		
		if (isset ( $reset ) && $reset) {
			$this->session->unset_userdata ( 'search_' . $this->current_class_name );
			$search = "";
			$listingPath = $controller_sub_folder . '/' . $this->current_class_name . "/" . $this->config->item ( "listing_view" );
			redirect ( base_url () . $listingPath );
		}
		if (isset ( $search ) && $search) {
			$this->session->set_userdata ( 'search_' . $this->current_class_name, $search );
			$limit_start = 0;
		}
		$search = $this->session->userdata ( 'search_' . $this->current_class_name );
		$condition = ($search != "") ? $search : "";
		
		/* search end */
		
		$total_items = $this->dashboard_model->listing ( $this->_xmldata, $tableField, $condition, $fields_sorting_saved_values, $orderByDirection, $orderByField, $limit_start, $limit_items, $additional_condition );
		
		$dashboard_helper = Dashboard_main_helper::get_instance();
		$total_items_count = $dashboard_helper->get('listing_total_items'); 
		$pagination_config = $this->_getPaginationConfig ( $per_page , $total_items_count );
		
		$this->pagination->initialize ( $pagination_config );
		
		$data ['paging'] = $this->pagination->create_links ();
		
		$viewPath = $this->current_class_name . "/" . $this->config->item ( "listing_file_name" );
		$viewPathAction = $this->current_class_name . "/" . $this->config->item ( "listing_action_file_name" );
		$footer_path = $this->current_class_name . "/" . $this->config->item ( "listing_footer_file_name" );
		
		if (! $this->view_exists ( $viewPath )) {
			$viewPath = $this->getCoreViewPath ( 'list' );
		}
		if (! $this->view_exists ( $viewPathAction )) {
			$viewPathAction = $this->getCoreViewPath ( 'list_action' );
		}
		if (! $this->view_exists ( $footer_path )) {
			$footer_path = $this->getCoreViewPath ( 'list_footer' );
		}
		
		$data ['total_items_count'] = $pagination_config ['total_rows'];
		$data ['permission'] = count ( $userPermissionDataArray );
		$data ['viewPathAction'] = $viewPathAction;
		$data ['viewPathFooter'] = $footer_path;
		$data ['per_page_show'] = $per_page;
		$data ['search'] = $this->input->post("search");
		$data ['all_field'] = $allField;
		$data ['table_name'] = $this->current_class_name;
		$data ['sorting_options'] = $order_by;
		$data ['list'] = $total_items;
		$data ['edit_path'] = $controller_sub_folder . '/' . $this->current_class_name . "/" . $this->config->item ( "edit_view" );
		$data ['delete_path'] = $controller_sub_folder . '/' . $this->current_class_name . "/" . $this->config->item ( "delete_view" );
		$data ['listing_path'] = $controller_sub_folder . '/' . $this->current_class_name . "/" . $this->config->item ( "listing_view" );
		$data ['class_name'] = $this->current_class_name;
		$data ['delete_method'] = $this->config->item ( "delete_view" );
		$data ['copy_method'] = $this->config->item ( "copy_view" );
		$data ['ajax_update_user_selection'] = $this->config->item ( "ajax_update_user_selection" );
		$data ['ajax_reset_user_selection'] = $this->config->item ( "ajax_reset_user_selection" );
		
		$data ['ajax_update_user_order'] = $this->config->item ( "ajax_update_user_order" );
		$data ['ajax_reset_user_order'] = $this->config->item ( "ajax_reset_user_order" );
		
		$data ['primary_key'] = $primary_key;
		
		$listing_field_ellipsis_length = intval ( $this->config->item ( "listing_field_ellipsis_length" ) );
		if ($listing_field_ellipsis_length == 0)
			$listing_field_ellipsis_length = 20;
		
		$data ['listing_field_ellipsis_length'] = $listing_field_ellipsis_length;
		
		$this->template->write_view ( 'content', $viewPath, $data );
		
		$this->template->render ();
	}
	/*
	 * check whether view exists
	 */
	protected function view_exists($view_name) {
		$file_path = FCPATH . APPPATH . 'views/' . $view_name . '.php';
		return file_exists ( $file_path );
	}
	/*
	 * get path for the core view
	 */
	protected function getCoreViewPath($type) {
		$core_view = $this->config->item ( 'core_view' );
		
		return $core_view [$type];
	}
	/*
	 * order by query
	 */
	protected function _getOrderBy($primary_key) {
		$field_to_sort = $this->input->post ( 'table_sort_field', '' );
		$sorting_direction = $this->input->post ( 'table_sort_direction', 'ASC' );
		$return = array (
				'field',
				'direction' 
		);
		if (strlen ( $field_to_sort )) {
			$return ['table_sort_field'] = $field_to_sort;
			$return ['table_sort_direction'] = $sorting_direction;
			$this->session->set_userdata ( 'table_sort_field', $field_to_sort );
			$this->session->set_userdata ( 'table_sort_direction', $sorting_direction );
			$this->save_userstate ( $field_to_sort, $sorting_direction );
		} else {
			$field_to_sort = $this->session->userdata ( 'table_sort_field', '' );
			$sorting_direction = $this->session->userdata ( 'table_sort_direction', 'ASC' );
			if (strlen ( $field_to_sort )) {
				$return ['table_sort_field'] = $field_to_sort;
				$return ['table_sort_direction'] = $sorting_direction;
			} else {
				$return ['table_sort_field'] = $primary_key;
				$return ['table_sort_direction'] = 'DESC';
			}
		}
		return $return;
	}
	/*
	 * saves users choice for sorting any field
	 */
	public function save_userstate($field_name, $direction) {
		$this->session->set_userdata ( 'userstate.' . $this->tableName . '.' . $field_name, $direction );
	}
	/*
	 * retrives user's choice for any particular field
	 */
	public function get_userstate($field_name) {
		$saved_value = $this->session->userdata ( 'userstate.' . $this->tableName . '.' . $field_name );
		if (strlen ( $saved_value )) {
			return $saved_value;
		} else {
			return $this->config->item ( 'all_order_by_value' );
		}
	}
	
	/*
	 * perform add and edit action
	 */
	public function edit($id = 0) {
		$supperUserPermission = $this->dashboard_model->getSupperUserPermission ( );
		
		$dashboard_helper = Dashboard_main_helper::get_instance ();
		$dashboard_helper->set ( "supper_user", 0 );
		$dashboard_helper->set ( "supper_user_edit_permission", 0 );
		
		$supper_user_edit_permission =0 ; 
		
		if( is_object($supperUserPermission) ){
			
			$dashboard_helper->set ( "supper_user", 1 );
			
			$dashboard_helper->set ( "supper_user_edit_permission", $supperUserPermission->edit );
			
			$supper_user_edit_permission = $supperUserPermission->edit ;
		}
		
		$supper_user = $dashboard_helper->get ( "supper_user" );
		
		$controller_sub_folder = $this->config->item ( 'controller_sub_folder' );
		

		
		$this->load->helper ( 'html' );
		$this->load->helper ( 'form' );
		
		$xmlData = $this->xml_parsing ();
		
		$tableName = $this->tablePrefix . $xmlData ['name'];
		
		$primary_key = (string) $xmlData ['primary_key'];
		$created_on = (string) $xmlData ['fieldname_created_on'];
		$modified_on = (string) $xmlData ['fieldname_modified_on'];
		
		/*
		 * GET user permission data
		 */
		$tableNameFixed = ( string ) $xmlData ["name"];
		$userPermissionDataArray = $this->getUserPermissionDataArray ( $tableNameFixed );
		
		
		if (array_key_exists ( "*", $userPermissionDataArray )) {
			
			$dashboard_helper->set ( "supper_user", 1 );
			$dashboard_helper->set ( "supper_user_edit_permission", $userPermissionDataArray['*'] );
			$supper_user=1;
			$supper_user_edit_permission =  $userPermissionDataArray['*'];
		}
			
		
		
		$data_edit = array ();
		
		$post = $this->input->post ();
		
		if (isset ( $post ) && $post) {
			
			$data = array ();
			$fieldDefaultValue = array();
			
			foreach ( $xmlData->field as $value ) {
				$fieldName = ( string ) $value ['name'];
				$fieldType = ( string ) $value ['type'];
				$fieldDefaultValue[$fieldName] = ( string ) $value ['default_value'];
				
				if (isset ( $fieldType ) && (($fieldType == "image") || ($fieldType == "file"))) {
					
					if (@$_FILES [$fieldName]) { 						
						$table_name = ( string ) $xmlData ['name'];
						
						$timestamp = strtotime ( "now" );
						$fileName = preg_replace("/[^a-zA-Z0-9.]+/", "", $_FILES[$fieldName]['name']);
												
						if($_FILES[$fieldName]['name'] != "" && ($fieldType != "file")){
							$imageExtension = strtolower(end(explode('.', $_FILES[$fieldName]['name']) ) );
							$config_allows_file_type = $this->config->item('img_upload_allowed_types');
							
							if(! in_array($imageExtension, $config_allows_file_type) ){
								$controller_sub_folder = $this->config->item ( 'controller_sub_folder' );
								$this->session->set_flashdata ( 'flash_message', dashboard_lang('_DADHBOARD_IMAGE_UPLOAD_TYPE_ERROR') );
							
								$this->session->set_userdata("dashboard_application_message_type", "error");
								redirect ( base_url () . $controller_sub_folder.'/'.$this->current_class_name.'/'.$this->config->item('edit_view').'/'.($id?$id:'') );
								exit();
							}
						}

						if (strlen ( $_FILES [$fieldName] ['name'] ) > 0) {
							$uploadedfileurl = $this->do_upload ( $table_name, $fileName, $fieldName, $timestamp, $fieldType, $id );
							
							$data = array_merge ( $data, array (
									$fieldName => $uploadedfileurl 
							) );
						}
					}
				} else if( isset ( $fieldType ) && ($fieldType == "datetime") ) {				
					
					if ( (is_array ( $userPermissionDataArray ) && count ( $userPermissionDataArray ) > 0) OR ($supper_user)) {
						$fieldName = ( string ) $value ['name'];
						
						if (array_key_exists ( $fieldName, $userPermissionDataArray )) {
							
							if($userPermissionDataArray[$fieldName]){
								
								$data = array_merge ( $data, array (
										$fieldName => (trim( $this->input->post ( $fieldName ) ) == '') ? $fieldDefaultValue[$fieldName] : strtotime(trim( $this->input->post ( $fieldName ) ) )
								) );
								
							}
							
						}else if ( $supper_user) {
							
							if($supper_user_edit_permission){
									
								$data = array_merge ( $data, array (
										$fieldName => (trim( $this->input->post ( $fieldName ) ) == '') ? $fieldDefaultValue[$fieldName] : strtotime(trim( $this->input->post ( $fieldName ) ) )
								) );
							}
						}
					}
					
					
				} else {
					
					
					if ( (is_array ( $userPermissionDataArray ) && count ( $userPermissionDataArray ) > 0) OR ($supper_user)) {
						$fieldName = ( string ) $value ['name'];
						
						if (array_key_exists ( $fieldName, $userPermissionDataArray )) {
							
							if($userPermissionDataArray[$fieldName]){
								
								$data = array_merge ( $data, array (
										$fieldName => (trim( $this->input->post ( $fieldName ) ) == '') ? $fieldDefaultValue[$fieldName] : trim( $this->input->post ( $fieldName ) )
								) );
								
							}
							
						}else if ( $supper_user) {
							
							if($supper_user_edit_permission){
									
								$data = array_merge ( $data, array (
										$fieldName => (trim( $this->input->post ( $fieldName ) ) == '') ? $fieldDefaultValue[$fieldName] : trim( $this->input->post ( $fieldName ) )
								) );
							}
						}
					}
					
					
				}
			}
			
			
			
			
			if ($id > 0) {
				
				if(@$data [$primary_key]) {
					
					unset ( $data [$primary_key] );
					
				}
				if(@$modified_on) {
						
					$data [$modified_on] = strtotime('now');
						
				}
							
				if( count($data) ){
					
					
					$this->db->where ( $primary_key, $id );
					
					$result = $this->db->update ( $tableName, $data );
					
					if ($result) {						
							
						$message_success = dashboard_lang ( "SUCCESSFULL_EDIT_MESSAGE" );
							
						$this->session->set_flashdata ( 'flash_message', $message_success );
							
							
					}
				}				
				
				
				$edit_path = $controller_sub_folder . '/' . $this->current_class_name . "/".$this->config->item('edit_view').'/'.$id;
				
				redirect ( base_url () . $edit_path );
				
			} else {
				
				unset ( $data [$primary_key] );
				if(@$created_on) {
				
					$data [$created_on] = strtotime('now');
					$data [$modified_on] = strtotime('now');
				
				}				
				
				$edit_path = $controller_sub_folder . '/' . $this->current_class_name . "/".$this->config->item('edit_view') ;
				
				if(count($data)){
				
					$result = $this->db->insert ( $tableName, $data );
					
					if ($result) {
							
						$id = $this->db->insert_id();	

						$edit_path .= '/'.$id;
							
						$message_success = dashboard_lang ( "SUCCESSFULL_INSERT_MESSAGE" );
							
						$this->session->set_flashdata ( 'flash_message', $message_success );							
							
					}
					
				}			
				
				
				
				redirect ( base_url () . $edit_path );
			}
		}
		
		if ($id > 0) {
			
			$this->db->select ( "*" );
			$this->db->where ( $primary_key, $id );
			$queryEditData = $this->db->get ( $tableName );
			
			if ($queryEditData->num_rows () > 0) {
				
				foreach ( $queryEditData->result () as $row ) {
					
					foreach ( $xmlData->field as $value ) {
						
						$fieldName = ( string ) $value ['name'];
						
						$data_edit [$fieldName] = $row->$fieldName;
					}
				}
			}
			
			$data ['data_edit'] = $data_edit;
			$data ['id'] = $id;
		}
		
		$viewPathAction = $this->current_class_name . "/" . $this->config->item ( "editing_action_file_name" );
		
		if (! $this->view_exists ( $viewPathAction )) {
			$viewPathAction = $this->getCoreViewPath ( 'edit_action' );
		}

		$view_additional = $this->current_class_name . "/" . $this->config->item ( "edit_form_bottom");
		if($this->view_exists($view_additional)){
			$data['view_additional'] = $view_additional;
		}
		
		$data ['viewPathAction'] = $viewPathAction;
		$data ['edit_field_array'] = $userPermissionDataArray;
		$data ['data_load'] = $xmlData;
		
		$data ['form_name'] = $this->current_class_name;
		$data ['class_name'] = $this->current_class_name;
		$data ['delete_method'] = $this->config->item ( "delete_view" );
		$data ['copy_method'] = $this->config->item ( "copy_view" );		
		$data ['edit_path'] = $controller_sub_folder . '/' . $this->current_class_name . "/" . $this->config->item ( "edit_view" );
		$data ['listing_path'] = $this->current_class_name . "/" . $this->config->item ( "listing_view" );
		
		$viewPath = $this->current_class_name . "/" . $this->config->item ( "edit_view" );
		if (! $this->view_exists ( $viewPath )) {
			$viewPath = $this->getCoreViewPath ( 'edit' );
		}
		$this->template->write_view ( 'content', $viewPath, $data );
		$this->template->render ();
	}
	
	/*
	 * perform copy
	 */
	public function copy() {
		
		$xmlData = $this->xml_parsing ();
		
		$tableName = $this->tablePrefix . $xmlData ['name'];
		
		$idsArray = $this->input->post ( 'ids' );
		
		$totalCopyRows = count ( $idsArray );
		
		$copyField = $this->copy_field ( $xmlData );
		
		$copyResult = $this->dashboard_model->copy ( $tableName, $idsArray, $copyField );
		
		if ($copyResult) {
			
			if ($totalCopyRows == 1) {
				$message_success = sprintf ( dashboard_lang ( 'SUCCESSFULL_ITEM_COPY_MESSAGE' ), $totalCopyRows );
			} elseif ($totalCopyRows > 1) {
				$message_success = sprintf ( dashboard_lang ( 'SUCCESSFULL_ITEMS_COPY_MESSAGE' ), $totalCopyRows );
			} else {
			}
			
			$this->session->set_flashdata ( 'flash_message', $message_success );
		}
		
		return $copyResult;
	}
	
	/*
	 * perform delete
	 */
	public function delete() {
		
		$xmlData = $this->xml_parsing ();
		
		$tableName = $this->tablePrefix . $xmlData ['name'];
		
		$ids = $this->input->post ( 'ids' );
		
		$totalDeletedRows = count ( $ids );
		
		$deleteIds = implode ( ",", $ids );
		
		$deleteResult = $this->dashboard_model->delete ( $tableName, $deleteIds );
		
		if ($deleteResult) {
			
			if ($totalDeletedRows == 1) {
				$message_success = sprintf ( dashboard_lang ( 'SUCCESSFULL_ITEM_DELETE_MESSAGE' ), $totalDeletedRows );
			} elseif ($totalDeletedRows > 1) {
				$message_success = sprintf ( dashboard_lang ( 'SUCCESSFULL_ITEMS_DELETE_MESSAGE' ), $totalDeletedRows );
			} else {
			}
			
			$this->session->set_flashdata ( 'flash_message', $message_success );
		}
		
		return $deleteResult;
	}
	
	/*
	 * User select from show field for listing data view
	 */
	function ajax_update_user_selection() {
		
		$tableName = $this->tablePrefix . $this->config->item ( "user_show_listing_field_table" );
		
		$result_return = "";
		
		$fields = $this->input->post ( 'fields' );
		
		$field_names = implode ( ",", $fields );
		
		$xmlData = $this->xml_parsing ();
		$tableNameFixed = ( string ) $xmlData ['name'];
		
		$data = array (
				'user_id' => get_user_id (),
				'table_name' => $tableNameFixed,
				'list_fields' => $field_names 
		);
		
		$this->db->select ( "user_id" );
		$this->db->where ( "user_id", get_user_id () );
		$this->db->where ( "table_name", $tableNameFixed );
		$query = $this->db->get ( $tableName );
		
		if ($query->num_rows () > 0) {
			$whereArray = array (
					"user_id" => get_user_id (),
					"table_name" => $tableNameFixed 
			);
			$this->db->where ( $whereArray );
			$result_return = $this->db->update ( $tableName, $data );
		} else {
			$result_return = $this->db->insert ( $tableName, $data );
		}
		
		return $result_return;
	}
	
	/*
	 * User select from show field for ordering data view
	 */
	function ajax_update_user_order() {
		
		$tableName = $this->tablePrefix . $this->config->item ( "user_show_listing_field_table" );
		
		$result_return = "";
		
		$fields = $this->input->post ( 'fields' );
		
		$field_names = implode ( ",", $fields );
		
		$xmlData = $this->xml_parsing ();
		$tableNameFixed = ( string ) $xmlData ['name'];
		
		$data = array (
				'user_id' => get_user_id (),
				'table_name' => $tableNameFixed,
				'order_fields' => $field_names 
		);
		
		$this->db->select ( "user_id" );
		$this->db->where ( "user_id", get_user_id () );
		$this->db->where ( "table_name", $tableNameFixed );
		$query = $this->db->get ( $tableName );
		
		if ($query->num_rows () > 0) {
			
			$whereArray = array (
					"user_id" => get_user_id (),
					"table_name" => $tableNameFixed 
			);
			
			$result_return = $this->dashboard_model->edit ( $tableName, $data, $whereArray );
		} else {
			$result_return = $this->dashboard_model->add ( $tableName, $data );
		}
		
		return $result_return;
	}
	
	/*
	 * User reset from show field for listing data view
	 */
	function ajax_reset_user_selection() {
		
		$tableName = $this->tablePrefix . $this->config->item ( "user_show_listing_field_table" );
		
		$user_id = $this->input->post ( 'user' );
		
		$data = array (
				'list_fields' => '' 
		);
		
		$xmlData = $this->xml_parsing ();
		$tableNameFixed = ( string ) $xmlData ['name'];
		
		$whereArray = array (
				"user_id" => get_user_id (),
				"table_name" => $tableNameFixed 
		);
		
		$result_return = $this->dashboard_model->edit ( $tableName, $data, $whereArray );
		
		return $result_return;
	}
	
	/*
	 * User reset from show field for ordering data view
	 */
	function ajax_reset_user_order() {
		
		$result_return='';
		
		$tableName = $this->tablePrefix . $this->config->item ( "user_show_listing_field_table" );
		
		$user_id = $this->input->post ( 'user' );
		
		$data = array (
				'order_fields' => '' 
		);
		
		$xmlData = $this->xml_parsing ();
		$tableNameFixed = ( string ) $xmlData ['name'];
		
		$whereArray = array (
				"user_id" => get_user_id (),
				"table_name" => $tableNameFixed 
		);
		
		$result_return = $this->dashboard_model->edit ( $tableName, $data, $whereArray );
		
		return $result_return;
	}
	/*
	 * parse xml data
	 */
	protected function xml_parsing() {
		if (empty ( $this->_xmldata )) {
			$xmlFile = FCPATH . $this->config->item ( "xml_file_path" ) . DIRECTORY_SEPARATOR . $this->current_class_name . ".xml";
			libxml_use_internal_errors(true);
			$this->_xmldata = simplexml_load_file ( $xmlFile );
			if($this->_xmldata === false){ 
				/*
				 * xml parsing error, shown error message in below
				 */
				foreach(libxml_get_errors() as $error) {					
					echo dashboard_lang('_DASHBOARD_XML_PERSING_ERROR') . "\t", $error->message;
				}
				exit();
			}
			$this->tableName = $this->tablePrefix . $this->_xmldata ['name'];
			return $this->_xmldata;
		} else {
			return $this->_xmldata;
		}
	}
	/*
	 * copy fields
	 */
	private function copy_field($xmlObject) {
		$copyFields = "";
		
		foreach ( $xmlObject->field as $value ) {
			if (isset ( $value ['copy'] ) && $value ['copy'] == 'true') {
				$copyFields = ( string ) $value ['name'];
			}
		}
		
		return $copyFields;
	}
	
	/*
	 * Search auto suggest words here
	 */
	public function get_search_words($word) {
		$limit = $this->config->item ( 'search_auto_suggest_limit' );
		$max_length = $this->config->item ( 'search_auto_suggest_maxlength' );
		
		$xmlData = $this->xml_parsing ();
		$tableName = $this->tablePrefix . $xmlData ['name'];
		$tableField = $this->get_show_field ();
		
		$list = $this->dashboard_model->get_words ( $tableName, $tableField, $word, $limit );
		
		if ($list) {
		    
		    foreach ($list as &$item ){
		        if( strlen( $item->value ) > $max_length ){
		            $item->value = substr($item->value , 0 , $max_length ).'...';
		        }
		    }
		    
			echo json_encode ( $list );
			
		} else {
		    
			echo json_encode ( array () );
			
		}
	}
	
	/*
	 * Show field will return fields form database or xml
	 */
	private function get_show_field() {
		$tableNameSelectFields = $this->tablePrefix . $this->config->item ( "user_show_listing_field_table" );
		;
		$tableField = array (
				"id" 
		);
		
		$this->db->select ( "list_fields" );
		
		$xmlData = $this->xml_parsing ();
		$tableNameFixed = ( string ) $xmlData ['name'];
		
		$whereArray = array (
				"user_id" => get_user_id (),
				"table_name" => $tableNameFixed 
		);
		$this->db->where ( $whereArray );
		$querySelectFields = $this->db->get ( $tableNameSelectFields );
		
		$querySelectResult = $querySelectFields->result ();
		
		if ($querySelectFields->num_rows () > 0 && strlen ( @$querySelectResult->list_fields )) {
			$row = $querySelectFields->row ();
			$tableFieldDb = explode ( ",", $row->list_fields );
			$tableField = array_merge ( $tableField, $tableFieldDb );
		} else {
			
			foreach ( $xmlData->field as $value ) {
				if (isset ( $value ['show'] ) && intval ( $value ['show'] ) > 0) {
					$tableField [] = ( string ) $value ['name'];
				}
			}
		}
		
		return $tableField;
	}
	/*
	 * to get the pagination config
	 */
	protected function _getPaginationConfig( $per_page , $total_items_count ) {
		$controller_sub_folder = $this->config->item ( 'controller_sub_folder' );
		
		$config ['uri_segment'] = 4;
		
		$config ['base_url'] = site_url () . '/' . $controller_sub_folder . '/' . $this->current_class_name . "/" . $this->config->item ( "listing_view" );
		$config ['total_rows'] = $total_items_count;
		$config ['per_page'] = $per_page;
		
		$config ['cur_tag_open'] = '<li class="paginate_button active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		
		$config ['first_tag_open'] = '<li class="paginate_button">';
		$config ['first_tag_close'] = '</li>';
		
		$config ['prev_tag_open'] = '<li class="paginate_button">';
		$config ['prev_tag_close'] = '</li>';
		
		$config ['full_tag_open'] = '<ul class="pagination">';
		$config ['full_tag_close'] = '</ul>';
		
		$config ['num_tag_open'] = '<li class="paginate_button">';
		$config ['num_tag_close'] = '</li>';
		$config ['next_tag_open'] = '<li class="paginate_button">';
		$config ['next_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li class="paginate_button">';
		$config ['last_tag_close'] = '</li>';
		
		return $config;
	}
	/*
	 * function to upload 
	 * image 
	 */
	protected function do_upload($table_name, $filename, $fieldName, $timestamp, $fieldType = "", $id = 0 ) {
		$img_upload_path = $this->config->item ( "img_upload_path" );
		$img_upload_max_size = $this->config->item ( "img_upload_max_size" );
		$file_upload_max_size = $this->config->item ( "file_upload_max_size" );
		$img_file_allowed_types = $this->config->item ( "img_file_allowed_types" );
		$file_allowed_types = $this->config->item('file_allowed_types');
		$uploadpath = $img_upload_path;
		
		$uploadpathWithFC = FCPATH . $img_upload_path;
		
		if (! is_dir ( $uploadpathWithFC )) {
			mkdir ( $uploadpathWithFC, 0777 );
		}
		
		$config = array ();
		
		$config ['upload_path'] = $uploadpath;
		
		$config ['allowed_types'] = '*';
		
		if ($fieldType == "image") {
			
			$config ['max_size'] = $img_upload_max_size;
			$config ['max_width'] = '1024';
			$config ['max_height'] = '768';
		}
		if ($fieldType == "file") {			
			$config ['max_size'] = $file_upload_max_size;
		}		
		$config ['file_name'] = $timestamp . "_" . $filename;
		
		
		$filethere = $uploadpath . $config ['file_name'];
		if (file_exists ( $filethere )) {
			// if file already exists
			unlink ( $filethere );
		}
		
		$this->load->library ( 'upload', $config );
		
		if (! $this->upload->do_upload ( $fieldName )) {
			$error = array (
					'error' => $this->upload->display_errors () 
			);
			$controller_sub_folder = $this->config->item ( 'controller_sub_folder' );
			$this->session->set_flashdata ( 'flash_message', dashboard_lang('_DASHBOARD_UPLOAD_INVALID_FILESIZE') );
				
			$this->session->set_userdata("dashboard_application_message_type", "error");
			redirect ( base_url () . $controller_sub_folder.'/'.$this->current_class_name.'/'.$this->config->item('edit_view').'/'.($id?$id:'') );
			exit();			
		} else {
			$dataUpload = array (
					'upload_data' => $this->upload->data () 
			);
			
			if ($fieldType == "image") {
				
				$thumbImageSizeHeight = $this->config->item ( "img_thumbnail_size_height" );
				$thumbImageSizeWidth = $this->config->item ( "img_thumbnail_size_width" );
				
				/*
				 * check if thumb exists 
				 * if not then create thumb folder 
				 */
				$thumb_path = $uploadpathWithFC.'thumbs';
				if (! is_dir ( $thumb_path )) {
					
					mkdir ( $thumb_path, 0777 );
					
				}
				
				$config_thumb = array (
						'image_library' => 'gd2',
						'source_image' => $dataUpload ['upload_data'] ['full_path'],
						'new_image' => $dataUpload ['upload_data'] ['file_path'] . "thumbs/",
						'maintain_ratio' => TRUE,
						'create_thumb' => TRUE,
						'thumb_marker' => '',
						'height' => $thumbImageSizeHeight,
						'width' => $thumbImageSizeWidth						
				);
				$this->load->library ( 'image_lib', $config_thumb );
				
				if (! $this->image_lib->resize ()) {
					$controller_sub_folder = $this->config->item ( 'controller_sub_folder' );									
					$this->session->set_flashdata ( 'flash_message', $this->image_lib->display_errors () );
					
					redirect ( base_url () . $controller_sub_folder.'/'.$this->current_class_name.'/'.$this->config->item('edit_view').'/'.($id?$id:'') );
					exit();									
				} else {
					$this->image_lib->clear ();
				}
			}
			
			return $timestamp . "_" . $filename;
		}
	}
	protected function userGroupPermission() {
		$xmlData = $this->xml_parsing ();
		
		$tableName = ( string ) $xmlData ["name"];
		
		$userQuery = $this->dashboard_model->getUserGroupQuery ( $tableName );
		
		if ($userQuery->num_rows () > 0) {
			
			foreach ( $userQuery->result () as $row ) {
				
				if ($row->field_name == '*') {
					$this->session->set_userdata ( $this->current_class_name . "_user_view_permission", $row->field_name );
					$this->session->set_userdata ( $this->current_class_name . "_user_edit_permission", $row->edit );
				} else {
					
					if ($row->edit > 0) {
						$this->session->set_userdata ( $this->current_class_name . "_user_edit_permission", $row->edit );
					}
					$this->session->set_userdata ( $this->current_class_name . "_user_view_permission", $row->field_name );
				}
			}
		}
		
		return true;
	}
	/*
	 * function to render a page where no permission 
	 * given
	 */
	public function permission() {
		$viewPath = $this->current_class_name . "/" . $this->config->item ( "no_permission" );
		
		if (! $this->view_exists ( $viewPath )) {
			$viewPath = $this->getCoreViewPath ( 'permission' );
		}
		
		$data ["no_permission_message"] = dashboard_lang ( "NO_PERMISSION" );
		
		$this->template->write_view ( 'content', $viewPath, $data );
		
		$this->template->render ();
	}
	/*
	 * function to provide permission
	 */
	protected  function getUserPermissionDataArray($tableNameFixed, $edit = "") {
		$userPermittedFields = array ();
		
		$selectField = "*";
		$whereArray = array (
				"role" => get_user_role (),
				"table" => $tableNameFixed 
		);
		
		if ($edit != "") {
			$where = array (
					"edit" => $edit 
			);
			$whereArray = array_merge ( $where, $whereArray );
		}
		
		$tableNameSelect = $this->tablePrefix . "table_permissions";
		
		$userDataQuery = $this->dashboard_model->get ( $tableNameSelect, $selectField, $whereArray );
		
		foreach ( $userDataQuery->result () as $value ) {
			$userPermittedFields [$value->field_name] = $value->edit ;
		}
		
		return $userPermittedFields;
	}
	
	/*
	 * Search auto suggest lookup here
	*/
	public function get_lookup_auto_suggest(){
	
		$limit = $this->config->item('search_auto_suggest_limit');
	
		$getTable = $this->input->get('tbl');		
		$tableName = $this->tablePrefix . $getTable;

		$word = $this->input->get('q');
		$id = $this->input->get('id');
		
		$key = $this->input->get('key');
		$val = $this->input->get('val');
		
		$orderBy = $val;
		$orderOn = "ASC";
		
		$xmlData = $this->xml_parsing();
		foreach ( $xmlData->field as $value ) {
			$fieldType = (string) $value ['type']; 
			if (isset ( $value ['type'] ) && $fieldType == 'lookup' && isset ( $value ['autosuggest'] ) && $value ['autosuggest'] == 1) {
				$keyXml = (string) $value['key'];
				$valueXml = (string) $value['value'];
				$refTableXml = (string) $value['ref_table'];
				if($keyXml == $key && $valueXml == $val && $refTableXml == $getTable){
					$orderBy = (string) $value['order_by'] ? (string) $value['order_by'] : $valueXml;
					$orderOn = (string) $value['order_on'] ? (string) $value['order_on'] : 'ASC';
				}
			}
						
		}	
		
		$tableField = array($key .' as id', $val . ' as name');
		
		if($id != ""){
			$wordWhere = array($key => $id);
		}

		if($word != ""){
			$wordWhere = array($val => $word);
		}
	
		$list = $this->dashboard_model->get_autosuggest_lookup($tableName, $tableField, $wordWhere, $orderBy, $orderOn, $limit);
		
		if ($list) {
			echo json_encode($list);
		} else {
			echo json_encode(array());
		}

		return '';
	}	
	

}


/* End of file: DashboardController.php */
/* Location: application/core/DashboardController.php */